import { createStore } from "vuex";
import axios from 'axios';
export default createStore({
  state: {
    counter: 0,
    colorCode: 'blue'
  },
  mutations: {
    decreaseCounter(state, randomNum){
      state.counter-=randomNum
    },
    increaseCounter(state, randomNum){
      state.counter+=randomNum
    },
    setColor(state, newVal){
      state.colorCode=newVal
    }
  },
  actions: {
    increaseCounter({commit}){
      console.log('increaseCounter(action)')
      axios('https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new').then(response => {
        commit('increaseCounter',response.data)
      })
    },
    decreaseCounter({commit}){
      console.log('decreaseCounter(action)')
      axios('https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new').then(response => {
        commit('decreaseCounter',response.data)
      })
    },
    setColor({commit}, newVal){
      commit('setColor', newVal)
    }
  },
  getters: {
    couterSquared(state) {
      return state.counter * state.counter
    }
  },
  modules: {},
});
